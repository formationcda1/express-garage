const logRequest = (req, res, next) => {
    //affichage la date, la methode HTTP et l'URL de la requête
    console.log(`${new Date().toLocaleString()} : ${req.method} ${req.originalUrl}`);
    //passe au middleware suivant
    next();
};

//export
module.exports = {logRequest};