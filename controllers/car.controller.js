const express = require('express');
const router = express.Router();
const {validateCarData} = require('../validator.middleware')
// Tu auras besoin de ta connexion à la base de données pour faire les requêtes SQL
const connection = require("../conf/db");

// Déplacer et adapter les routes vers /cars
router.get('/', (req, res) => {
    connection.query('SELECT * FROM car', (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération des voitures');
        } else {
            res.json(results);
        }
    });
});

router.post('/', validateCarData, (req, res) => {
    const { brand, model } = req.body;
    // Exécution d'une requête SQL pour insérer 
    connection.execute('INSERT INTO car (brand, model) VALUES (?, ?)', [brand, model], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de l\'ajout du véhicule');
        } else {
            // Confirmation de l'ajout  avec l'ID du véhicule inséré
            res.status(201).send(`Véhicule ajouté avec l'ID ${results.insertId}`);
        }
    });
});

router.put('/:id', validateCarData, (req, res) => {
    const { brand, model } = req.body;
    const id = req.params.id;
    // Exécution d'une requête SQL pour mettre à jour les informations du véhicule spécifié
    connection.execute('UPDATE car SET brand = ?, model = ? WHERE car_id = ?', [brand, model, id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la mise à jour du véhicule');
        } else {

            res.send(`Véhicule mis à jour.`);
        }
    });
});


router.delete('/:id', (req, res) => {
    const id = req.params.id;

    connection.execute('DELETE FROM car WHERE car_id = ?', [id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la suppression du véhicule');
        } else {

            res.send(`Véhicule supprimé.`);
        }
    });
});

// Route GET pour récupérer un véhicule spécifique par sa marque en utilisant une requête préparée 
router.get('/:brand', (req, res) => {
    const brand = req.params.brand;
    // Utilisation d'une requête préparée avec un paramètre pour sécuriser l'accès aux données
    connection.execute('SELECT * FROM car WHERE brand = ?', [brand], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération de la marque');
        } else {
            // Envoi du résultat sous forme de JSON si la requête est réussie
            res.json(results);
        }
    });
});





module.exports = router;