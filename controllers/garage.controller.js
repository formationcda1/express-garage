const express = require('express');
const router = express.Router();

const connection = require('../conf/db');
const {validateGarageData} = require('../validator.middleware');
router.get('/', (req, res) => {
    connection.query(' SELECT * FROM garage ', (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération des garages')
        }
        else {
            res.json(results)
        }
    })
})

router.post('/', validateGarageData, (req, res) => {
    const { name, email } = req.body
    connection.execute('INSERT INTO garage (name, email) VALUES (name = ?, email = ?)', [name, email], (err, results) => {
        if (err) {
            res.status(500).send({ error: err.message })
        }
        else {
            res.status(200).json(req.body)
        }
    })
})

router.put('/:id', validateGarageData, (req, res) => {
    const { name, email } = req.body;
    const id = req.params.id;
    // Exécution d'une requête SQL pour mettre à jour les informations d'un garage
    connection.execute('UPDATE garage SET name = ?, email = ? WHERE garage_id = ?', [name, email, id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la mise à jour du garage');
        } else {

            res.send(`Garage mis à jour.`);
        }
    });
});

router.delete('/:id', (req, res) => {
    const id = req.params.id;
    connection.execute(' DELETE FROM garage WHERE garage_id = ?', [id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la suppression')
        } else {
            res.status(200).json(results)
        }
    })
})

module.exports = router;