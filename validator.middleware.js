const validateCarData = (req, res, next) => {
    const { brand, model } = req.body;

    // Vérifier que la marque et le model sont présent dans la requête
    if (!brand || !model) {
        return res.status(400).json({ message: 'La marque et le model sont obligatoires.' });
    }
}

const validateGarageData = (req, res, next) => {
    const { name, email } = req.body;

    //Vérifier que le nom et l'email sont présent dans la requete
    if (!name || !email) {
        return res.status(400).json({ message: 'Le nom et l\'email sont obligatoires.' });
    }

     // Valider le format de l'email à l'aide de la regex
     const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
     if (!emailRegex.test(email)) {
         return res.status(400).json({ message: 'Format d\'email incorrect.' });
     }

    //passe au middleware suivant
    next();
};

module.exports = { validateCarData, validateGarageData };