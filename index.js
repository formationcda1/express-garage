require('dotenv').config();

const express = require('express');
// Importe les modules pour interagir avec des bases de données MySQL.
const connection = require('./conf/db');
// import de la bibiothèque cors
const cors = require("cors");
const app = express();

// Configure l'application pour parser les requêtes avec des corps au format JSON.
app.use(express.json());

// Configure l'application pour parser les requêtes avec des corps encodés en URL (utile pour les formulaires web).
app.use(
    express.urlencoded({
        extended: true,
    })
);

const port = 3000;

// ajout d'une configuration pour autoriser une origine particulière 
app.use(cors({
    origin: 'http://localhost:8080'
}));

//Import du middleware logger
const {logRequest} = require('./logger.middleware');
const {validateGarageData} = require('./validator.middleware')
//Utilise le middleware pour logger les requêtes
app.use(logRequest);
app.use(validateGarageData);
//Importe les contrôleurs 
const garagesController = require('./controllers/garage.controller');
const carsController= require('./controllers/car.controller');

//Utiliser le contrôleur pour gérer pour gérer les routes des cars
app.use('/garage', garagesController);
app.use('/cars', carsController);

// Établissement de la connexion
connection.getConnection((err) => {
    if (err instanceof Error) {
        console.log('getConnection error:', err);
        return;
    }
});

const garageController = require('./controllers/garage.controller');
app.use('/garages', garageController);
const carController = require('./controllers/car.controller');
app.use('/cars', carController);
// définition d'une route GET sur l'URL racine ('/')
app.get('/', (req, res) => {
    // envoie une réponse 'Hello World!' au client
    res.send('Hello World!');
})


app.get('/cars', (req, res) => {
    connection.query('SELECT * FROM car', (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération des voitures');
        } else {
            res.json(results);
        }
    });
});

app.post('/cars', (req, res) => {
    const { brand, model } = req.body;
    // Exécution d'une requête SQL pour insérer 
    connection.execute('INSERT INTO car (brand, model) VALUES (?, ?)', [brand, model], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de l\'ajout du véhicule');
        } else {
            // Confirmation de l'ajout  avec l'ID du véhicule inséré
            res.status(201).send(`Véhicule ajouté avec l'ID ${results.insertId}`);
        }
    });
});

app.put('/cars/:id', (req, res) => {
    const { brand, model } = req.body;
    const id = req.params.id;
    // Exécution d'une requête SQL pour mettre à jour les informations du véhicule spécifié
    connection.execute('UPDATE car SET brand = ?, model = ? WHERE car_id = ?', [brand, model, id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la mise à jour du véhicule');
        } else {

            res.send(`Véhicule mis à jour.`);
        }
    });
});


app.delete('/cars/:id', (req, res) => {
    const id = req.params.id;

    connection.execute('DELETE FROM car WHERE car_id = ?', [id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la suppression du véhicule');
        } else {

            res.send(`Véhicule supprimé.`);
        }
    });
});

// Route GET pour récupérer un véhicule spécifique par sa marque en utilisant une requête préparée 
app.get('/cars/:brand', (req, res) => {
    const brand = req.params.brand;
    // Utilisation d'une requête préparée avec un paramètre pour sécuriser l'accès aux données
    connection.execute('SELECT * FROM car WHERE brand = ?', [brand], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération de la marque');
        } else {
            // Envoi du résultat sous forme de JSON si la requête est réussie
            res.json(results);
        }
    });
});


// Démarre le serveur Express et écoute sur le port spécifié
app.listen(port, () => {
    console.log(`Serveur en écoute sur le port ${port}`);
});




